<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title>Document</title>
    <style>
        .error-message {
            color: #ff0000;
            padding-top: 5px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <form action="{{ route('generate.pdf') }}">
                        @method('GET')
                        @csrf()
                        <div class="form-group">
                             <label for="">Date</label>
                             <input type="date" name="date" class="form-control">
                             @if($errors->has('date'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('date') }}</p>
                                </div>
                             @endif
                        </div>
                        <div class="form-group">
                            <label for="">Contract No.</label>
                            <input type="text" name="contract_no" class="form-control">
                            @if($errors->has('contract_no'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('contract_no') }}</p>
                                </div>
                             @endif
                       </div>
                       <div class="form-group">
                            <label for="">Seller Name</label>
                            <input type="text" name="seller_name" class="form-control">
                            @if($errors->has('seller_name'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('seller_name') }}</p>
                                </div>
                             @endif
                        </div>
                        <div class="form-group">
                            <label for="">Project Start</label>
                            <input type="date" name="project_start" class="form-control">
                            @if($errors->has('project_start'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('project_start') }}</p>
                                </div>
                             @endif
                        </div>
                        <div class="form-group">
                            <label for="">Contracts for</label>
                            <select name="contracts_for" id="" class="form-control">
                                <option value="36">3 years</option>
                                <option value="60">5 years</option>
                            </select>
                            @if($errors->has('contracts_for'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('contracts_for') }}</p>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Project price</label>
                            <input type="number" name="project_price" class="form-control">
                            @if($errors->has('project_price'))
                                <div class="error">
                                    <p class="error-message">{{ $errors->first('project_price') }}</p>
                                </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-block btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
