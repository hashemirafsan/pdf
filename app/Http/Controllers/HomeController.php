<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LynX39\LaraPdfMerger\Facades\PdfMerger;


class HomeController extends Controller {

    public function showForm()
    {
        return view('form.pdf-form');
    }

    public function generatePDF(Request $request)
    {
        $this->validate($request, [
            'date'          => 'required',
            'seller_name'   => 'required',
            'contract_no'   => 'required',
            'project_start' => 'required',
            'project_price' => 'required',
            'contracts_for' => 'required',
        ]);
        
        $data = $request->only(['date', 'seller_name', 'contract_no', 'project_start', 'project_price', 'contracts_for']);
        $data['per_installment'] = round($request->get('project_price') / $request->get('contracts_for'), 2);

        $data = array_merge($data, [
            'date' => Carbon::parse($data['date']),
            'project_start' => Carbon::parse($data['project_start'])->startOfMonth(),
        ]);

        $pdfMerger = PDFMerger::init();
        $pdfMerger->addPDF(public_path('pdf/title.pdf'), 'all');

        $pdf = \PDF::loadView('pdf.invoice', compact('data'));
        $pdf->save(public_path('pdf/generate.pdf'));

        $pdfMerger->addPDF(public_path('pdf/generate.pdf'), 'all');
        $pdfMerger->addPDF(public_path('pdf/title-2.pdf'), '6-20');
        $pdfMerger->merge();


        return $pdfMerger->save(public_path('pdf/001.pdf'), "browser");
    }

}
